#Imports:
import pygame
import sys
import os
sys.path.append(os.path.abspath(""))
import models

#Consts and globals:
LOOP = True
SPEED = 3
B_SIZE = 5
SHOOTED = False

#Initialize screen:
pygame.init()

size = width, height = 0, 0

screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

size = pygame.display.get_surface().get_size()

clock = pygame.time.Clock()

pygame.mouse.set_visible(True)

sprite_list = []

pos = 0
#Objects:
player = models.Player((255,0,0))

#Functions:
def keyPressMovement(player):
    keys_pressed = pygame.key.get_pressed()

    if keys_pressed[pygame.K_LEFT]:
        player.location = (player.location[0] - SPEED, player.location[1])

    if keys_pressed[pygame.K_RIGHT]:
        player.location = (player.location[0] + SPEED, player.location[1])

    if keys_pressed[pygame.K_UP]:
        player.location = (player.location[0], player.location[1] - SPEED)

    if keys_pressed[pygame.K_DOWN]:
        player.location = (player.location[0], player.location[1] + SPEED)

def collisionHandler(player):
    if (player.location[0] + player.size) > size[0]:
        player.location = size[0] - player.size, player.location[1]
        
    if (player.location[0] - player.size) < 0:
        player.location = 0 + player.size, player.location[1]
        
    if (player.location[1] + player.size) > size[1]:
        player.location = player.location[0], size[1] - player.size
        
    if (player.location[1] - player.size) < 0:
        player.location = player.location[0], 0 + player.size

def shoot(player, pos):
    loc = player.location
    bullet = models.Bullet(loc, pos)
    sprite_list.append(bullet)
    SHOOTED = False
        

#Game loop:
while LOOP:
    #Screen handler:
    clock.tick(60)
    screen.fill((255,255,255))
    
    #Create:
    pygame.draw.circle(screen, player.color, player.location, player.size)
    for bullet in sprite_list:
        if bullet.shooted:
            bullet.update()
            pygame.draw.circle(screen, bullet.color, bullet.location, B_SIZE)
    pygame.display.flip()
    
    #Collision handler:
    collisionHandler(player)
    
    #Event handler:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            LOOP = False
            sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()
            SHOOTED = True

    #Movement handler
    keyPressMovement(player)

    #Shoot was made
    if SHOOTED:
        shoot(player, pos)



