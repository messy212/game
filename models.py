#Imports
import pygame

#Consts

#Classes
class Player:
    size = 25
    location = (300, 300)

    def __init__(self, color):
        self.color = color

class Bullet(pygame.sprite.Sprite):
    location = (0,0)
    speed = 5
    shooted = True
    color = (0,255,0)
    angle = 0
    mouse_pos = 0
    
    def __init__(self, location, mouse_pos):
        self.location = location
        self.mouse_pos = mouse_pos
        self.angle = (location[1] - mouse_pos[1]) / (location[0] - mouse_pos[0])
        
    def update(self):
        if self.shooted:
            if self.location[0] < self.mouse_pos[0]:
                self.location = (self.location[0] + self.speed, int(self.location[0]*self.angle)) 
            elif self.location[0] > self.mouse_pos[0]:
                self.location = (self.location[0] + self.speed, int(self.location[0]*self.angle))

