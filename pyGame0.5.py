import pygame
import sys
from twisted.internet.task import LoopingCall
from twisted.internet import reactor
DESIRED_FPS = 60.0 # 30 frames per second

class Player:
    color = (0, 0, 255)
    size = 25
    location = (300, 300)

pygame.init()

size = width, height = 600, 600

screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

size = pygame.display.get_surface().get_size()

clock = pygame.time.Clock()

player2 = Player()
player2.color = (255, 0, 0)
player2.location = (150, 150)
player1 = Player()

LOOP = False
while LOOP:
    clock.tick(60)

def game_tick():
    screen.fill((56,163,234))
    if (player1.location[0] + player1.size) > size[0]:
        player1.location = size[0] - player1.size, player1.location[1]
    if (player1.location[0] - player1.size) < 0:
        player1.location = 0 + player1.size, player1.location[1]
    if (player1.location[1] + player1.size) > size[1]:
        player1.location = player1.location[0], size[1] - player1.size
    if (player1.location[1] - player1.size) < 0:
        player1.location = player1.location[0], 0 + player1.size

    if (player2.location[0] + player2.size) > size[0]:
        player2.location = size[0] - player2.size, player2.location[1]
    if (player2.location[0] - player2.size) < 0:
        player2.location = 0 + player2.size, player2.location[1]
    if (player2.location[1] + player2.size) > size[1]:
        player2.location = player2.location[0], size[1] - player2.size
    if (player2.location[1] - player2.size) < 0:
        player2.location = player2.location[0], 0 + player2.size

    pygame.draw.circle(screen, player1.color, player1.location, player1.size)
    pygame.draw.circle(screen, player2.color, player2.location, player2.size)
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            LOOP = False
            sys.exit()

    keys_pressed = pygame.key.get_pressed()

    if keys_pressed[pygame.K_a]:
        player2.location = (player2.location[0] - 10, player2.location[1])

    if keys_pressed[pygame.K_d]:
        player2.location = (player2.location[0] + 10, player2.location[1])

    if keys_pressed[pygame.K_w]:
        player2.location = (player2.location[0], player2.location[1] - 10)

    if keys_pressed[pygame.K_s]:
        player2.location = (player2.location[0], player2.location[1] + 10)


    if keys_pressed[pygame.K_LEFT]:
        player1.location = (player1.location[0] - 10, player1.location[1])

    if keys_pressed[pygame.K_RIGHT]:
        player1.location = (player1.location[0] + 10, player1.location[1])

    if keys_pressed[pygame.K_UP]:
        player1.location = (player1.location[0], player1.location[1] - 10)

    if keys_pressed[pygame.K_DOWN]:
        player1.location = (player1.location[0], player1.location[1] + 10)
    #redraw()

# Set up a looping call every 1/30th of a second to run your game tick
tick = LoopingCall(game_tick)
tick.start(1.0 / DESIRED_FPS)

# Set up anything else twisted here, like listening sockets

reactor.run() # Omit this if this is a tap/tac file